<?php

/**
 * @file
 * Configuration for autodeployment.
 */

namespace Deployer;

require 'recipe/drupal8.php';

set('repository', 'git@gitlab.com:slobaprodanovic/eRanch.git');
set('http_user', 'www-data');
set('keep_releases', 3);

// Drupal 8 shared dirs.
set(
  'shared_dirs',
  [
    'web/sites/{{drupal_site}}/files',
    'private',
  ]
);

// Drupal 8 shared files.
set('shared_files', [
  'web/sites/{{drupal_site}}/settings.local.php',
  'web/.htaccess',
]);

// Drupal 8 Writable dirs.
set('writable_dirs', [
  'web/sites/{{drupal_site}}/files',
  'private',
]);

// Install composer dependencies.
desc('Installing vendors');
task('deploy:composer', static function () {
  if (!commandExist('unzip')) {
    writeln('<comment>To speed up composer installation setup "unzip" command with PHP zip extension https://goo.gl/sxzFcD</comment>');
  }
  run('cd {{release_path}} && {{bin/composer}} {{composer_options}}');
});
after('deploy:shared', 'deploy:composer');

// Install submodules.
desc('Install submodules');
task('deploy:install_submodules', static function () {
  if (commandExist('git')) {
    run('cd {{release_path}} && git submodule update --init');
  }
});
after('deploy:composer', 'deploy:install_submodules');

// Clear cache with drush.
desc('First cleaning cache');
task('deploy:first_clear_cache', static function () {
  if (commandExist('drush')) {
    run('cd {{release_path}}/web && drush cr');
  }
});
after('deploy:install_submodules', 'deploy:first_clear_cache');

// Implement configuration.
desc('Import configuration');
task('deploy:import_configuration', static function () {
  if (commandExist('drush')) {
    run('cd {{release_path}}/web && drush -y cim');
  }
});
after('deploy:first_clear_cache', 'deploy:import_configuration');

// Import per environment config.
desc('Import configuration for dev environment');
task('deploy:import_configuration_dev', static function () {
  if (commandExist('drush')) {
    run('cd {{release_path}}/web && drush config-split:import config_development');
  }
})->local()->onStage('dev');
after('deploy:import_configuration', 'deploy:import_configuration_dev');
desc('Import configuration for live environment');
task('deploy:import_configuration_live', static function () {
  if (commandExist('drush')) {
    run('cd {{release_path}}/web && drush config-split:import config_production');
  }
})->local()->onStage('live');
after('deploy:import_configuration', 'deploy:import_configuration_live');

// Clear cache with drush.
desc('Cleaning cache');
task('deploy:clear_cache', static function () {
  if (commandExist('drush')) {
    run('cd {{release_path}}/web && drush cr');
  }
});
after('deploy:import_configuration', 'deploy:clear_cache');

// Import hosts.
inventory('hosts.yml');

// Add current path to hosts.
host('gazdinstvo.slobaprodanovic.in.rs')
  ->set('current_path', '{{deploy_path}}/current');
host('www.gazdinstvo.net')
  ->set('current_path', '{{deploy_path}}/current');
