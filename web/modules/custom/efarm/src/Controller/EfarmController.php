<?php

namespace Drupal\efarm\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Farm routes.
 */
class EfarmController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
