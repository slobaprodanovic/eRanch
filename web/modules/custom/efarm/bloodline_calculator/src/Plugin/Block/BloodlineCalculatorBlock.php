<?php

namespace Drupal\bloodline_calculator\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *     id="blodline_calculator_block",
 *     admin_label=@Translation("Bloodline Calculator"),
 *     category=@Translation("Farm")
 * )
 */
class BloodlineCalculatorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = 'Prazno';
    $nid = $this->getNodeId();

    if ($nid) {
      $markup = $this->getAncestorsIds($nid);
    }

    $build['content'] = [
      '#markup' => $markup,
    ];

    return $build;
    // TODO: Implement build() method.
  }

  /**
   * Returns IDs of ancestors.
   *
   * @return string|true
   *   - add text later.
   */
  protected function getAncestorsIds(int $nid) {
    $counter = 0;

    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'animal');

    $results = $query->execute();

    return print_r($query->execute());

    do {
      $results = $query->execute();

      ++$counter;
    } while (($counter <= 10) && (!empty($results)));
  }

  /**
   * Gets Node ID.
   *
   * @return false|int
   *   - write later.
   */
  protected function getNodeId() {
    $arg = arg();

    if (isset($arg[0]) && ($arg[0] === 'node') &&
      isset($arg[1]) && is_numeric($arg[1])) {
      return (int) $arg[1];
    }

    return FALSE;
  }

}
